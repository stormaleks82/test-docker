ARG FLUENTD_IMAGE=fluent/fluentd:v1.15.3-debian-1.1

FROM ${FLUENTD_IMAGE} AS builder

ARG DEBIAN_FRONTEND=noninteractive

USER root

COPY Gemfile Gemfile.lock /fluentd/

RUN apt-get update && \
    apt-get install -y --no-install-recommends make gcc g++ libc-dev && \
    cd /fluentd && bundle install && \
    rm -rf /usr/local/bundle/cache/*.gem && \
    chmod -R o+rX /usr/local/bundle

###

FROM ${FLUENTD_IMAGE}

COPY --from=builder /usr/local/bundle/ /usr/local/bundle
COPY fluent.conf /etc/fluent/fluent.conf
COPY entrypoint.sh /bin/

ENV FLUENTD_CONF="../../etc/fluent/fluent.conf"
