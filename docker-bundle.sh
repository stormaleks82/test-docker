#!/usr/bin/env bash

set -euo pipefail

# Use this to update Gemfile.lock.
# usage: ./docker-bundle [subcommand. default: "install"]

docker run --rm -v "$(pwd):/fluentd" -w /fluentd -u root \
  "$(awk '/^ARG FLUENTD_IMAGE=/ {print $2; exit}' Dockerfile | cut -d= -f2)" \
  sh -c "apt-get update && apt-get install -y --no-install-recommends make gcc g++ libc-dev && bundle ${1:-install}"
